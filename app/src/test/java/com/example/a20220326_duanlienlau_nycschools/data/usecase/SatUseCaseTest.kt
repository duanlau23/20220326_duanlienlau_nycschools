package com.example.a20220326_duanlienlau_nycschools.data.usecase

import com.example.a20220326_duanlienlau_nycschools.data.mapper.SatApiResponseMapper
import com.example.a20220326_duanlienlau_nycschools.data.model.SatModel
import com.example.a20220326_duanlienlau_nycschools.domain.entity.SatEntity
import com.example.a20220326_duanlienlau_nycschools.domain.remote.response.ApiResponse
import com.example.a20220326_duanlienlau_nycschools.domain.remote.response.ErrorCode
import com.example.a20220326_duanlienlau_nycschools.domain.remote.response.ErrorResponse
import com.example.a20220326_duanlienlau_nycschools.domain.repository.SchoolRepository
import kotlinx.coroutines.*
import kotlinx.coroutines.test.resetMain
import kotlinx.coroutines.test.runTest
import kotlinx.coroutines.test.setMain
import org.junit.After
import org.junit.Assert
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.Mockito.*
import org.mockito.junit.MockitoJUnitRunner

@DelicateCoroutinesApi
@ExperimentalCoroutinesApi
@RunWith(MockitoJUnitRunner::class)
class SatUseCaseTest {

    @Mock
    lateinit var repository: SchoolRepository

    @Mock
    lateinit var mapper: SatApiResponseMapper

    private val mainThreadSurrogate = newSingleThreadContext("Main thread")

    lateinit var satUseCase: SatUseCase

    @Before
    fun setup() {
        Dispatchers.setMain(mainThreadSurrogate)
        satUseCase = SatUseCase(repository, mapper)
    }

    @After
    fun tearDown() {
        Dispatchers.resetMain()
        mainThreadSurrogate.close()
    }

    @Test
    fun `when the response is success but comes with an empty list`() = runTest {
        launch(Dispatchers.Main) {
            `when`(repository.getSAT(DBN)).thenReturn(EMPTY_RESPONSE)
            `when`(mapper.map(EMPTY_RESPONSE)).thenReturn(MAPPED_EMPTY_RESPONSE)
            val response = satUseCase.getSchools(DBN)
            verify(repository).getSAT(DBN)
            verify(mapper).map(EMPTY_RESPONSE)
            Assert.assertEquals(response, MAPPED_EMPTY_RESPONSE)
        }
    }

    @Test
    fun `when the response is success and comes with data`() = runTest {
        launch(Dispatchers.Main) {
            `when`(repository.getSAT(DBN)).thenReturn(FILLED_RESPONSE)
            `when`(mapper.map(FILLED_RESPONSE)).thenReturn(MAPPED_FILLED_RESPONSE)
            val response = satUseCase.getSchools(DBN)
            verify(repository).getSAT(DBN)
            verify(mapper).map(FILLED_RESPONSE)
            Assert.assertEquals(response, MAPPED_FILLED_RESPONSE)
        }
    }

    @Test
    fun `when the response is failed`() = runTest {
        launch(Dispatchers.Main) {
            `when`(repository.getSAT(DBN)).thenReturn(FAILED_RESPONSE)
            `when`(mapper.map(FAILED_RESPONSE)).thenReturn(MAPPED_FAILED_RESPONSE)
            val response = satUseCase.getSchools(DBN)
            verify(repository).getSAT(DBN)
            verify(mapper).map(FAILED_RESPONSE)
            Assert.assertEquals(response, MAPPED_FAILED_RESPONSE)
        }
    }

    companion object {
        private const val DBN = "123123"
        private val FAILED_RESPONSE = ApiResponse.Failure<List<SatEntity>>(ErrorResponse(ErrorCode.BAD_RESPONSE, "ERROR", "MESSAGE"))
        private val MAPPED_FAILED_RESPONSE = ApiResponse.Failure<List<SatModel>>(ErrorResponse(ErrorCode.BAD_RESPONSE, "ERROR", "MESSAGE"))
        private val EMPTY_RESPONSE: ApiResponse.Success<List<SatEntity>> = ApiResponse.Success(listOf())
        private val MAPPED_EMPTY_RESPONSE: ApiResponse.Success<List<SatModel>> = ApiResponse.Success(listOf())
        private val FILLED_RESPONSE: ApiResponse.Success<List<SatEntity>> = ApiResponse.Success(listOf(SatEntity("", "", "", "", "", "")))
        private val MAPPED_FILLED_RESPONSE: ApiResponse.Success<List<SatModel>> = ApiResponse.Success(listOf(SatModel("", "", "", "", "", "")))
    }

}