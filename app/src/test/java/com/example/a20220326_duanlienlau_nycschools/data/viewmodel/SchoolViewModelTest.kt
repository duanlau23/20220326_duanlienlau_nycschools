package com.example.a20220326_duanlienlau_nycschools.data.viewmodel

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.example.a20220326_duanlienlau_nycschools.data.model.SatModel
import com.example.a20220326_duanlienlau_nycschools.data.repository.SchoolRepositoryImpl
import com.example.a20220326_duanlienlau_nycschools.data.repository.SchoolRepositoryTest
import com.example.a20220326_duanlienlau_nycschools.data.usecase.SchoolsUseCase
import com.example.a20220326_duanlienlau_nycschools.domain.entity.SatEntity
import com.example.a20220326_duanlienlau_nycschools.domain.remote.response.ApiResponse
import com.example.a20220326_duanlienlau_nycschools.domain.remote.response.ErrorCode
import com.example.a20220326_duanlienlau_nycschools.domain.remote.response.ErrorResponse
import com.example.a20220326_duanlienlau_nycschools.presentation.viewmodel.SchoolViewModel
import kotlinx.coroutines.*
import kotlinx.coroutines.test.TestCoroutineDispatcher
import kotlinx.coroutines.test.resetMain
import kotlinx.coroutines.test.runTest
import kotlinx.coroutines.test.setMain
import org.junit.*
import org.junit.rules.TestRule
import org.junit.runner.RunWith
import org.mockito.junit.MockitoJUnitRunner
import org.mockito.kotlin.mock
import org.mockito.kotlin.verify
import org.mockito.kotlin.whenever

@DelicateCoroutinesApi
@ExperimentalCoroutinesApi
@RunWith(MockitoJUnitRunner::class)
class SchoolViewModelTest {

    @get:Rule var rule: TestRule = InstantTaskExecutorRule()

    val schoolsUseCase: SchoolsUseCase = mock()

    private lateinit var schoolViewModel: SchoolViewModel

    private val mainThreadSurrogate = newSingleThreadContext("Main thread")

    @Before
    fun setup() {
        Dispatchers.setMain(mainThreadSurrogate)

        schoolViewModel = SchoolViewModel(schoolsUseCase)
    }

    @After
    fun tearDown() {
        Dispatchers.resetMain()
    }

    @Test
    fun `when try to get the sat score but the response is an error`() = runTest {
        whenever(schoolsUseCase.getSchools())
            .thenReturn(ApiResponse.Failure(ErrorResponse(ErrorCode.BAD_RESPONSE, "ERROR", "MESSAGE")))
        schoolViewModel.getSchools()
        verify(schoolsUseCase).getSchools()
    }


    @Test
    fun `when try to get the sat score but the response is successful`() = runTest {
        whenever(schoolsUseCase.getSchools()).thenReturn(ApiResponse.Success(listOf()))
        schoolViewModel.getSchools()
        verify(schoolsUseCase).getSchools()
    }

}