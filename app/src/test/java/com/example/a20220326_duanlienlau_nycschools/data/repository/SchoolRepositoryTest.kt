package com.example.a20220326_duanlienlau_nycschools.data.repository

import com.example.a20220326_duanlienlau_nycschools.data.model.SatModel
import com.example.a20220326_duanlienlau_nycschools.domain.entity.SatEntity
import com.example.a20220326_duanlienlau_nycschools.domain.entity.SchoolEntity
import com.example.a20220326_duanlienlau_nycschools.domain.remote.response.ApiResponse
import com.example.a20220326_duanlienlau_nycschools.domain.remote.response.ErrorCode
import com.example.a20220326_duanlienlau_nycschools.domain.remote.response.ErrorResponse
import com.example.a20220326_duanlienlau_nycschools.domain.service.SchoolService
import kotlinx.coroutines.DelicateCoroutinesApi
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.newSingleThreadContext
import kotlinx.coroutines.test.resetMain
import kotlinx.coroutines.test.runTest
import kotlinx.coroutines.test.setMain
import okhttp3.ResponseBody
import org.junit.After
import org.junit.Assert
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.junit.MockitoJUnitRunner
import org.mockito.kotlin.mock
import org.mockito.kotlin.verify
import org.mockito.kotlin.whenever
import retrofit2.Response

@DelicateCoroutinesApi
@ExperimentalCoroutinesApi
@RunWith(MockitoJUnitRunner::class)
class SchoolRepositoryTest {

    private val service: SchoolService = mock()

    private lateinit var repository: SchoolRepositoryImpl

    private val mainThreadSurrogate = newSingleThreadContext("Main thread")

    @Before
    fun setup() {
        Dispatchers.setMain(mainThreadSurrogate)
        repository = SchoolRepositoryImpl(service)
    }

    @After
    fun tearDown() {
        Dispatchers.resetMain()
        mainThreadSurrogate.close()
    }

    @Test
    fun `when try to get the sat score but the response is an error`() = runTest {
        whenever(repository.getSAT(DBN)).thenReturn(ApiResponse.Failure(ErrorResponse(ErrorCode.UNKNOWN)))
        val response = repository.getSAT(DBN)
        verify(service).getSAT(DBN)
        Assert.assertTrue(!response.isSuccessful)
    }


    @Test
    fun `when try to get the sat score but the response comes empty`() = runTest {
        whenever(service.getSAT(DBN)).thenReturn(Response.success(EMPTY_SAT_ENTITIES_LIST))
        val response = repository.getSAT(DBN)
        verify(service).getSAT(DBN)
        Assert.assertEquals(response.data, EMPTY_SAT_ENTITIES_LIST)
    }

    @Test
    fun `when try to get the sat score and the response comes filled`() = runTest {
        whenever(service.getSAT(DBN)).thenReturn(Response.success(FILLED_SAT_ENTITIES_LIST))
        val response = repository.getSAT(DBN)
        verify(service).getSAT(DBN)
        Assert.assertEquals(response.data, FILLED_SAT_ENTITIES_LIST)
    }


    @Test
    fun `when try to get the schools but the response comes empty`() = runTest {
        whenever(service.getSchools()).thenReturn(Response.success(EMPTY_SCHOOL_ENTITIES_LIST))
        val response = repository.getSchools()
        verify(service).getSchools()
        Assert.assertEquals(response.data, EMPTY_SCHOOL_ENTITIES_LIST)
    }

    @Test
    fun `when try to get the schools and the response comes filled`() = runTest {
        whenever(service.getSchools()).thenReturn(Response.success(FILLED_SCHOOL_ENTITIES_LIST))
        val response = repository.getSchools()
        verify(service).getSchools()
        Assert.assertEquals(response.data, FILLED_SCHOOL_ENTITIES_LIST)
    }

    companion object {
        private const val DBN = "123123"
        private val EMPTY_SAT_ENTITIES_LIST = listOf<SatEntity>()
        private val FILLED_SAT_ENTITIES_LIST = listOf(SatEntity())
        private val EMPTY_SCHOOL_ENTITIES_LIST = listOf<SchoolEntity>()
        private val FILLED_SCHOOL_ENTITIES_LIST = listOf(SchoolEntity())
    }

}