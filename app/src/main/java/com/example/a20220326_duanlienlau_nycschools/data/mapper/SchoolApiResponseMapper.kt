package com.example.a20220326_duanlienlau_nycschools.data.mapper

import com.example.a20220326_duanlienlau_nycschools.data.mapper.base.BaseMapper
import com.example.a20220326_duanlienlau_nycschools.data.model.SchoolModel
import com.example.a20220326_duanlienlau_nycschools.domain.entity.SchoolEntity
import com.example.a20220326_duanlienlau_nycschools.domain.remote.response.ApiResponse
import com.example.a20220326_duanlienlau_nycschools.domain.remote.response.ErrorResponse
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class SchoolApiResponseMapper @Inject constructor(private val mapper : SchoolEntityToModelMapper)
    : BaseMapper<ApiResponse<List<SchoolEntity>>, ApiResponse<List<SchoolModel>>>() {

    override fun map(entity: ApiResponse<List<SchoolEntity>>?): ApiResponse<List<SchoolModel>> {
        entity?.let {
            return if (it.isSuccessful) {
                ApiResponse.Success(mapper.map(it.data ?: listOf()))
            } else {
                ApiResponse.Failure(it.errorResponse)
            }
        }
        return ApiResponse.Failure(entity?.errorResponse ?: ErrorResponse())
    }
}