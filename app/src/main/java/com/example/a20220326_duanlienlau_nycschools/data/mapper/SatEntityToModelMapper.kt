package com.example.a20220326_duanlienlau_nycschools.data.mapper

import com.example.a20220326_duanlienlau_nycschools.data.mapper.base.BaseMapper
import com.example.a20220326_duanlienlau_nycschools.data.model.SatModel
import com.example.a20220326_duanlienlau_nycschools.domain.entity.SatEntity
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class SatEntityToModelMapper @Inject constructor() : BaseMapper<SatEntity, SatModel>() {

    override fun map(entity: SatEntity?): SatModel? {
        entity?.let {
            return SatModel(
                it.dbn,
                it.schoolName,
                it.numOfSatTestTakers,
                it.satCriticalReadingAvgScore,
                it.satMathAvgScore,
                it.satWritingAvgScore
            )
        }
        return null
    }
}