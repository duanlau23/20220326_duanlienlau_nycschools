package com.example.a20220326_duanlienlau_nycschools.data.mapper

import com.example.a20220326_duanlienlau_nycschools.data.mapper.base.BaseMapper
import com.example.a20220326_duanlienlau_nycschools.data.model.SatModel
import com.example.a20220326_duanlienlau_nycschools.domain.entity.SatEntity
import com.example.a20220326_duanlienlau_nycschools.domain.remote.response.ApiResponse
import com.example.a20220326_duanlienlau_nycschools.domain.remote.response.ErrorResponse
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class SatApiResponseMapper @Inject constructor(private val mapper : SatEntityToModelMapper)
    : BaseMapper<ApiResponse<List<SatEntity>>, ApiResponse<List<SatModel>>>() {

    override fun map(entity: ApiResponse<List<SatEntity>>?): ApiResponse<List<SatModel>> {
        entity?.let {
            return if (it.isSuccessful) {
                ApiResponse.Success(mapper.map(it.data ?: listOf()))
            }
            else {
                ApiResponse.Failure(it.errorResponse)
            }
        }
        return ApiResponse.Failure(entity?.errorResponse ?: ErrorResponse())
    }
}