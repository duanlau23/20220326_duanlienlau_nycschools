package com.example.a20220326_duanlienlau_nycschools.data.usecase

import com.example.a20220326_duanlienlau_nycschools.data.mapper.SatApiResponseMapper
import com.example.a20220326_duanlienlau_nycschools.data.model.SatModel
import com.example.a20220326_duanlienlau_nycschools.domain.remote.response.ApiResponse
import com.example.a20220326_duanlienlau_nycschools.domain.repository.SchoolRepository
import com.example.a20220326_duanlienlau_nycschools.domain.usecase.base.BaseUseCase
import javax.inject.Inject

class SatUseCase @Inject constructor(
    private val repository: SchoolRepository,
    private val mapper : SatApiResponseMapper
) : BaseUseCase(repository) {

    suspend fun getSchools(dbn: String): ApiResponse<List<SatModel>> {
        val response = repository.getSAT(dbn)
        return mapper.map(response)
    }
}