package com.example.a20220326_duanlienlau_nycschools.data.repository

import com.example.a20220326_duanlienlau_nycschools.domain.entity.SatEntity
import com.example.a20220326_duanlienlau_nycschools.domain.entity.SchoolEntity
import com.example.a20220326_duanlienlau_nycschools.domain.remote.response.ApiResponse
import com.example.a20220326_duanlienlau_nycschools.domain.repository.SchoolRepository
import com.example.a20220326_duanlienlau_nycschools.domain.service.SchoolService
import javax.inject.Inject

class SchoolRepositoryImpl @Inject constructor(private val service: SchoolService) : SchoolRepository() {

    override suspend fun getSchools(): ApiResponse<List<SchoolEntity>> {
        return handleRequest { service.getSchools() }
    }

    override suspend fun getSAT(dbn: String): ApiResponse<List<SatEntity>> {
        return handleRequest { service.getSAT(dbn) }
    }
}