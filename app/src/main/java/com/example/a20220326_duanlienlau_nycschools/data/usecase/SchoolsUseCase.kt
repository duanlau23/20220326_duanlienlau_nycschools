package com.example.a20220326_duanlienlau_nycschools.data.usecase

import com.example.a20220326_duanlienlau_nycschools.data.mapper.SchoolApiResponseMapper
import com.example.a20220326_duanlienlau_nycschools.data.model.SchoolModel
import com.example.a20220326_duanlienlau_nycschools.domain.remote.response.ApiResponse
import com.example.a20220326_duanlienlau_nycschools.domain.repository.SchoolRepository
import com.example.a20220326_duanlienlau_nycschools.domain.usecase.base.BaseUseCase
import javax.inject.Inject

class SchoolsUseCase @Inject constructor(
    private val repository: SchoolRepository,
    private val mapper : SchoolApiResponseMapper
) : BaseUseCase(repository) {

    suspend fun getSchools(): ApiResponse<List<SchoolModel>> {
        val response = repository.getSchools()
        return mapper.map(response)
    }
}