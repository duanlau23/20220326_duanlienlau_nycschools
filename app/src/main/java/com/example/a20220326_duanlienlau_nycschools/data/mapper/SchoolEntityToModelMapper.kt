package com.example.a20220326_duanlienlau_nycschools.data.mapper

import com.example.a20220326_duanlienlau_nycschools.data.mapper.base.BaseMapper
import com.example.a20220326_duanlienlau_nycschools.data.model.SchoolModel
import com.example.a20220326_duanlienlau_nycschools.domain.entity.SchoolEntity
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class SchoolEntityToModelMapper @Inject constructor() : BaseMapper<SchoolEntity, SchoolModel>() {

    override fun map(entity: SchoolEntity?): SchoolModel? {
        entity?.let {
            return SchoolModel(
                it.dbn,
                it.boro,
                it.name,
                it.overviewParagraph,
                it.schoolTenthSeats,
                it.primaryOpportunities,
                it.secondaryOpportunities)
        }
        return null
    }
}