package com.example.a20220326_duanlienlau_nycschools.presentation.fragment.base

import android.content.Context
import android.content.DialogInterface
import androidx.fragment.app.FragmentManager
import com.example.a20220326_duanlienlau_nycschools.presentation.listener.BottomSheetDisplayListener
import com.google.android.material.bottomsheet.BottomSheetDialog
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import dagger.android.support.AndroidSupportInjection.inject

open class BaseBottomSheetDialog: BottomSheetDialogFragment() {

    private var displayListener: BottomSheetDisplayListener? = null

    override fun onAttach(context: Context) {
        inject(this)
        super.onAttach(context)
    }

    override fun show(manager: FragmentManager, tag: String?) {
        displayListener?.displayChanged(true)
        val ft = manager.beginTransaction()
        ft.add(this, tag)
        ft.commitAllowingStateLoss()
    }

    override fun onDismiss(dialog: DialogInterface) {
        super.onDismiss(dialog)
        displayListener?.displayChanged(false)
    }

    override fun dismiss() = dismissAllowingStateLoss()

    fun setBottomSheetDisplayListener(displayListener: BottomSheetDisplayListener) {
        this.displayListener = displayListener
    }
}