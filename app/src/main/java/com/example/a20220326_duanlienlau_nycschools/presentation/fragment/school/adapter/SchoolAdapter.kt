package com.example.a20220326_duanlienlau_nycschools.presentation.fragment.school.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.a20220326_duanlienlau_nycschools.data.model.SchoolModel
import com.example.a20220326_duanlienlau_nycschools.databinding.ItemSchoolBinding
import com.example.a20220326_duanlienlau_nycschools.presentation.listener.OnItemClickListener

class SchoolAdapter(
    private var schoolList: List<SchoolModel> = listOf(),
    private val clickListener: OnItemClickListener<SchoolModel>
): RecyclerView.Adapter<SchoolViewHolder>() {

    fun setData(newSchoolList: List<SchoolModel>) {
        this.schoolList = newSchoolList
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): SchoolViewHolder {
        val binding = ItemSchoolBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return SchoolViewHolder(binding, clickListener)
    }

    override fun onBindViewHolder(holder: SchoolViewHolder, position: Int) {
        holder.bind(schoolList[position])
    }

    override fun getItemCount() = schoolList.size
}

class SchoolViewHolder(
    private val binding: ItemSchoolBinding,
    private val clickListener: OnItemClickListener<SchoolModel>
): RecyclerView.ViewHolder(binding.root) {
    fun bind(data: SchoolModel) {
        binding.dbnTextView.text = data.dbn
        binding.nameTextView.text = data.boro

        itemView.setOnClickListener {
            clickListener.onItemClick(data)
        }
    }
}