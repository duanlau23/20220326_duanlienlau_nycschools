package com.example.a20220326_duanlienlau_nycschools.presentation.activity.school

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.widget.doOnTextChanged
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.a20220326_duanlienlau_nycschools.common.logger.Logger
import com.example.a20220326_duanlienlau_nycschools.data.model.SchoolModel
import com.example.a20220326_duanlienlau_nycschools.databinding.FragmentSchoolBinding
import com.example.a20220326_duanlienlau_nycschools.presentation.fragment.school.SchoolDetailBottomSheet
import com.example.a20220326_duanlienlau_nycschools.presentation.fragment.school.adapter.SchoolAdapter
import com.example.a20220326_duanlienlau_nycschools.presentation.listener.BottomSheetDisplayListener
import com.example.a20220326_duanlienlau_nycschools.presentation.listener.OnItemClickListener
import com.example.a20220326_duanlienlau_nycschools.presentation.viewmodel.SchoolViewModel
import dagger.android.DispatchingAndroidInjector
import dagger.android.HasAndroidInjector
import javax.inject.Inject

private const val TAG = "SchoolActivity"

class SchoolActivity :
    AppCompatActivity(),
    HasAndroidInjector,
    BottomSheetDisplayListener,
    OnItemClickListener<SchoolModel> {

    private lateinit var binding: FragmentSchoolBinding

    @Inject
    lateinit var dispatchingAndroidInjector: DispatchingAndroidInjector<Any>

    @Inject
    lateinit var viewModel: SchoolViewModel

    private var bottomSheetIsDisplayed = false

    private val _adapter = SchoolAdapter(listOf(), this)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        observeSchoolChanges()
        binding = FragmentSchoolBinding.inflate(
            LayoutInflater.from(this)
        )
        setContentView(binding.root)

        binding.loader.visibility = View.VISIBLE
        binding.editTextSearch.doOnTextChanged { text, _, _, _ ->
            if (!text.isNullOrEmpty()) {
                viewModel.getSchoolsByDBNOrName(binding.editTextSearch.text.toString())
            } else {
                viewModel.resetData()
            }
        }

        binding.schoolsRecyclerView.apply {
            adapter = _adapter
            layoutManager = LinearLayoutManager(this.context)
            addItemDecoration(DividerItemDecoration(context, LinearLayoutManager.VERTICAL))
        }
        viewModel.getSchools()
    }

    private fun showSchoolDetail(selectedSchool: SchoolModel) {
        if (!bottomSheetIsDisplayed) {
            SchoolDetailBottomSheet.newInstance(selectedSchool).apply {
                setBottomSheetDisplayListener(this@SchoolActivity)
            }.show(
                supportFragmentManager,
                SchoolDetailBottomSheet.TAG
            )
        }
    }

    private fun observeSchoolChanges() {
        viewModel.schoolData.observe(this) {
           it?.let { schoolList ->
               _adapter.setData(schoolList)
               binding.loader.visibility = View.GONE
           }
        }

        viewModel.tempSchoolData.observe(this) {
            it?.let { schoolList ->
                _adapter.setData(schoolList)
            }
        }

        viewModel.schoolFailedData.observe(this) {
            Toast.makeText(this, "Error: $it", Toast.LENGTH_SHORT).show()
            Logger.Error(TAG, "Todo data fetch failed -> $it")
        }
    }

    override fun displayChanged(isDisplayed: Boolean) {
        bottomSheetIsDisplayed = isDisplayed
    }

    override fun onItemClick(item: SchoolModel) {
        showSchoolDetail(item)
    }

    override fun androidInjector() = dispatchingAndroidInjector
}