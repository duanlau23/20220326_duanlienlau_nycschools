package com.example.a20220326_duanlienlau_nycschools.presentation.listener

interface BottomSheetDisplayListener {

    fun displayChanged(isDisplayed: Boolean)

}