package com.example.a20220326_duanlienlau_nycschools.presentation.viewmodel.base

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel

open class BaseViewModel : ViewModel() {

    /**
     * Request state
     **/
    val apiRequestInProgress = MutableLiveData<Boolean>()

    /**
     * Sets the initial request state to false
     **/
    init {
        apiRequestInProgress.value = false
    }
}