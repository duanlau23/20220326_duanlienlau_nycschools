package com.example.a20220326_duanlienlau_nycschools.presentation.viewmodel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.example.a20220326_duanlienlau_nycschools.common.liveevent.SingleLiveEvent
import com.example.a20220326_duanlienlau_nycschools.data.model.SchoolModel
import com.example.a20220326_duanlienlau_nycschools.data.usecase.SchoolsUseCase
import com.example.a20220326_duanlienlau_nycschools.domain.remote.response.ErrorResponse
import com.example.a20220326_duanlienlau_nycschools.presentation.viewmodel.base.BaseViewModel
import kotlinx.coroutines.launch
import javax.inject.Inject

class SchoolViewModel @Inject constructor(
    private val schoolsUseCase: SchoolsUseCase
) : BaseViewModel() {

    val schoolData = MutableLiveData<List<SchoolModel>?>()
    val schoolFailedData = SingleLiveEvent<ErrorResponse?>()
    val tempSchoolData = MutableLiveData<List<SchoolModel>?>()

    fun getSchools() {
        apiRequestInProgress.value = true
        schoolData.value = null
        viewModelScope.launch {
            val response = schoolsUseCase.getSchools()
            if(response.isSuccessful) {
                schoolData.value = response.data
            } else {
                schoolFailedData.value = response.errorResponse
            }
            apiRequestInProgress.value = false
        }
    }

    fun getSchoolsByDBNOrName(query: String) {
        if (query.isNotEmpty()) {
            tempSchoolData.value = schoolData.value?.filter {
                it.boro.containsNotNull(query) ||
                it.dbn.containsNotNull(query)
            } ?: schoolData.value
        } else {
            tempSchoolData.value = schoolData.value
        }
    }

    fun resetData() {
        tempSchoolData.value = schoolData.value
    }
}

fun String?.containsNotNull(regex: String): Boolean =
    if (this.isNullOrEmpty()) {
        false
    } else {
        lowercase().contains(regex.lowercase())
    }