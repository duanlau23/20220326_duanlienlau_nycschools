package com.example.a20220326_duanlienlau_nycschools.presentation.viewmodel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.example.a20220326_duanlienlau_nycschools.common.liveevent.SingleLiveEvent
import com.example.a20220326_duanlienlau_nycschools.data.model.SatModel
import com.example.a20220326_duanlienlau_nycschools.data.usecase.SatUseCase
import com.example.a20220326_duanlienlau_nycschools.domain.remote.response.ErrorResponse
import com.example.a20220326_duanlienlau_nycschools.presentation.viewmodel.base.BaseViewModel
import kotlinx.coroutines.launch
import javax.inject.Inject

class SchoolDetailViewModel @Inject constructor(
    private val satUseCase: SatUseCase
) : BaseViewModel() {


    val satData = MutableLiveData<List<SatModel>?>()
    val satDataFailureEvent = SingleLiveEvent<ErrorResponse?>()


    /**
     * Obtain the SAT scores using the dbn
     * @param dbn is the unique school id
     **/
    fun getSAT(dbn: String) {
        apiRequestInProgress.value = true
        satData.value = null
        viewModelScope.launch {
            // get data from network
            val response = satUseCase.getSchools(dbn)
            // check network response
            if(response.isSuccessful) {
                // set data value
                satData.value = response.data
            } else {
                // notify error event
                satDataFailureEvent.value = response.errorResponse
            }
            // mark api request in progress to false
            apiRequestInProgress.value = false
        }
    }
}