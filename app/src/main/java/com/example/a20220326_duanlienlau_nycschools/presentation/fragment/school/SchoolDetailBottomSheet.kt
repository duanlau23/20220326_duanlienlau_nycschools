package com.example.a20220326_duanlienlau_nycschools.presentation.fragment.school

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.os.bundleOf
import com.example.a20220326_duanlienlau_nycschools.R
import com.example.a20220326_duanlienlau_nycschools.data.model.SatModel
import com.example.a20220326_duanlienlau_nycschools.data.model.SchoolModel
import com.example.a20220326_duanlienlau_nycschools.databinding.BottomSheetSchoolDetailBinding
import com.example.a20220326_duanlienlau_nycschools.presentation.viewmodel.SchoolDetailViewModel
import com.example.a20220326_duanlienlau_nycschools.presentation.fragment.base.BaseBottomSheetDialog
import dagger.android.DispatchingAndroidInjector
import dagger.android.HasAndroidInjector
import javax.inject.Inject

class SchoolDetailBottomSheet: BaseBottomSheetDialog(), HasAndroidInjector {

    @Inject
    lateinit var dispatchingAndroidInjector: DispatchingAndroidInjector<Any>

    @Inject
    lateinit var schoolDetailViewModel: SchoolDetailViewModel

    private lateinit var binding: BottomSheetSchoolDetailBinding

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = BottomSheetSchoolDetailBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        arguments?.getParcelable<SchoolModel>(SELECTED_SCHOOL)?.let { selectedSchool ->
            fillPreData(selectedSchool)
            selectedSchool.dbn?.let {
                schoolDetailViewModel.getSAT(it)
            }
        }
        schoolDetailViewModel.satData.observe(this) {
            fillData(it?.firstOrNull())
        }
    }

    private fun fillPreData(schoolModel: SchoolModel) {
        binding.textViewSchoolName.text = schoolModel.boro
        binding.textViewDbn.text = schoolModel.dbn
    }

    private fun fillData(satModel: SatModel?) {
        if (satModel != null) {
            binding.textViewNoSatData.visibility = View.GONE
            binding.textViewNumOfTakers.text = getString(R.string.num_of_takers, satModel.numOfSatTestTakers)
            binding.textViewReadingAvg.text = getString(R.string.reading_avg, satModel.satCriticalReadingAvgScore)
            binding.textViewMathAvg.text = getString(R.string.math_avg, satModel.satMathAvgScore)
            binding.textViewWritingAvg.text = getString(R.string.writing_avg, satModel.satWritingAvgScore)
        } else {
            binding.textViewNoSatData.visibility = View.VISIBLE
        }
    }

    override fun androidInjector() = dispatchingAndroidInjector

    companion object {
        const val TAG = "SchoolDetailBottomSheet"
        private const val SELECTED_SCHOOL = "selected_school"

        fun newInstance(
            selectedSchool: SchoolModel
        ): SchoolDetailBottomSheet {
            return SchoolDetailBottomSheet().apply {
                arguments = bundleOf(
                    SELECTED_SCHOOL to selectedSchool
                )
            }
        }
    }
}