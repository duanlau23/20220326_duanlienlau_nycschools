package com.example.a20220326_duanlienlau_nycschools.presentation.listener

interface OnItemClickListener <ITEM> {

    fun onItemClick(item: ITEM)

}