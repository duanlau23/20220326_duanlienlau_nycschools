package com.example.a20220326_duanlienlau_nycschools.domain.repository


import com.example.a20220326_duanlienlau_nycschools.domain.entity.SatEntity
import com.example.a20220326_duanlienlau_nycschools.domain.entity.SchoolEntity
import com.example.a20220326_duanlienlau_nycschools.domain.remote.response.ApiResponse
import com.example.a20220326_duanlienlau_nycschools.domain.repository.base.IRepository

abstract class SchoolRepository() : IRepository() {

    abstract suspend fun getSchools() : ApiResponse<List<SchoolEntity>>


    abstract suspend fun getSAT(dbn: String) : ApiResponse<List<SatEntity>>
}