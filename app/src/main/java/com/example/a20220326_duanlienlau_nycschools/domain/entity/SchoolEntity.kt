package com.example.a20220326_duanlienlau_nycschools.domain.entity

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.parcelize.Parcelize


@Parcelize
data class SchoolEntity(
    @SerializedName("dbn")
    val dbn: String ?= "",
    @SerializedName("school_name")
    val name: String ?= "",
    @SerializedName("boro")
    val boro: String ?= "",
    @SerializedName("overview_paragraph")
    val overviewParagraph: String ?= "",
    @SerializedName("school_10th_seats")
    val schoolTenthSeats: Long ?= 0,
    @SerializedName("academicopportunities1")
    val primaryOpportunities: String ?= "",
    @SerializedName("academicopportunities2")
    val secondaryOpportunities: String ?= ""
): Parcelable