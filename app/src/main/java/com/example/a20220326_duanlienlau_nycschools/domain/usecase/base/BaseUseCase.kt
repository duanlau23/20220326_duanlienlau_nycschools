package com.example.a20220326_duanlienlau_nycschools.domain.usecase.base

import com.example.a20220326_duanlienlau_nycschools.domain.repository.base.IRepository

abstract class BaseUseCase(repository : IRepository)