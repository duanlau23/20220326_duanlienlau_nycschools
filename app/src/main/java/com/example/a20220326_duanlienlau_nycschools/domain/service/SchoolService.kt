package com.example.a20220326_duanlienlau_nycschools.domain.service

import com.example.a20220326_duanlienlau_nycschools.domain.entity.SatEntity
import com.example.a20220326_duanlienlau_nycschools.domain.entity.SchoolEntity
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Query

interface SchoolService {

    @GET("s3k6-pzi2")
    suspend fun getSchools() : Response<List<SchoolEntity>>

    @GET("f9bf-2cp4")
    suspend fun getSAT(@Query("dbn") dbn: String) : Response<List<SatEntity>>

}