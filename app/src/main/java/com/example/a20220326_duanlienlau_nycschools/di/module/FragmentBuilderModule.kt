package com.example.a20220326_duanlienlau_nycschools.di.module

import com.example.a20220326_duanlienlau_nycschools.presentation.fragment.school.SchoolDetailBottomSheet
import dagger.Module
import dagger.android.ContributesAndroidInjector

/**
 * Define all your fragments here
 */

@Module
interface FragmentBuilderModule {

    @ContributesAndroidInjector
    fun contributeSchoolDetailBottomSheet(): SchoolDetailBottomSheet

}