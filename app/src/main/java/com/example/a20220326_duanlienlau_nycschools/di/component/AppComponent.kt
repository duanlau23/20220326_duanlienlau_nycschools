package com.example.a20220326_duanlienlau_nycschools.di.component

import android.app.Application
import com.example.a20220326_duanlienlau_nycschools.application.CoreApplication
import com.example.a20220326_duanlienlau_nycschools.di.module.ActivityBuilderModule
import com.example.a20220326_duanlienlau_nycschools.di.module.ViewModelModule
import com.example.a20220326_duanlienlau_nycschools.di.module.NetworkModule
import com.example.a20220326_duanlienlau_nycschools.di.module.ServiceModule
import com.example.a20220326_duanlienlau_nycschools.di.module.UseCaseModule
import dagger.BindsInstance
import dagger.Component
import dagger.android.AndroidInjectionModule
import javax.inject.Singleton

@Singleton
@Component(
    modules = [
        AndroidInjectionModule::class,
        ActivityBuilderModule::class,
        ViewModelModule::class,
        NetworkModule::class,
        ServiceModule::class,
        UseCaseModule::class
    ]
)
interface AppComponent {
    @Component.Builder
    interface Builder {

        @BindsInstance
        fun application(application: Application): Builder

        fun build(): AppComponent
    }

    fun inject(application: CoreApplication)
}