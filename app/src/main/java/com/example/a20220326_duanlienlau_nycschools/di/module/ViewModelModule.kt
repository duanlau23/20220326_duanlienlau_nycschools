package com.example.a20220326_duanlienlau_nycschools.di.module

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.example.a20220326_duanlienlau_nycschools.di.key.ViewModelKey
import com.example.a20220326_duanlienlau_nycschools.presentation.viewmodel.SchoolDetailViewModel
import com.example.a20220326_duanlienlau_nycschools.presentation.viewmodel.SchoolViewModel
import com.example.a20220326_duanlienlau_nycschools.presentation.viewmodel.ViewModelFactory
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap

@Module
interface ViewModelModule {

    @Binds
    fun bindViewModelFactory(viewModelFactory: ViewModelFactory): ViewModelProvider.Factory

    @Binds
    @IntoMap
    @ViewModelKey(SchoolViewModel::class)
    fun bindSchoolViewModel(viewModel: SchoolViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(SchoolDetailViewModel::class)
    fun bindSchoolDetailViewModel(viewModel: SchoolDetailViewModel): ViewModel
}