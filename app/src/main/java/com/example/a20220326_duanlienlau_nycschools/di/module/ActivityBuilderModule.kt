package com.example.a20220326_duanlienlau_nycschools.di.module

import com.example.a20220326_duanlienlau_nycschools.presentation.activity.school.SchoolActivity
import dagger.Module
import dagger.android.ContributesAndroidInjector

/**
 * Define all your activities here
 */

@Module(includes = [FragmentBuilderModule::class])
interface ActivityBuilderModule {

    @ContributesAndroidInjector
    fun contributeSchoolActivity(): SchoolActivity
}