package com.example.a20220326_duanlienlau_nycschools.di.module

import com.example.a20220326_duanlienlau_nycschools.domain.service.SchoolService
import dagger.Module
import dagger.Provides
import retrofit2.Retrofit
import javax.inject.Singleton

@Module
class ServiceModule {

    @Provides
    @Singleton
    fun provideSchoolService(retrofit: Retrofit) : SchoolService {
        return retrofit.create(SchoolService::class.java)
    }
}