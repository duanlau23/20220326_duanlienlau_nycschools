package com.example.a20220326_duanlienlau_nycschools.di.module

import com.example.a20220326_duanlienlau_nycschools.data.mapper.SatApiResponseMapper
import com.example.a20220326_duanlienlau_nycschools.data.mapper.SchoolApiResponseMapper
import com.example.a20220326_duanlienlau_nycschools.data.repository.SchoolRepositoryImpl
import com.example.a20220326_duanlienlau_nycschools.data.usecase.SatUseCase
import com.example.a20220326_duanlienlau_nycschools.data.usecase.SchoolsUseCase
import com.example.a20220326_duanlienlau_nycschools.domain.repository.SchoolRepository
import dagger.Binds
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module(includes = [RepositoryModule::class])
class UseCaseModule {

    @Provides
    @Singleton
    fun provideSchoolUseCase(repository: SchoolRepository, mapper: SchoolApiResponseMapper): SchoolsUseCase {
        return SchoolsUseCase(repository, mapper)
    }

    @Provides
    @Singleton
    fun provideSATUseCase(repository: SchoolRepository, mapper: SatApiResponseMapper): SatUseCase {
        return SatUseCase(repository, mapper)
    }
}

@Module
interface RepositoryModule {

    @Binds
    fun bindSchoolRepository(repository: SchoolRepositoryImpl): SchoolRepository

}